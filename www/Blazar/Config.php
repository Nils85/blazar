<?php
namespace Blazar;

# Web app configuration
class Config {
const

# This mode temporarily stops new downloads and uploads without stopping those that are in progress
MAINTENANCE_MODE = false,   # true or false

# Website title
WEBSITE_NAME = 'Blazar',

# Language by default if there is no translation available for the user
DEFAULT_LANGUAGE = 'en',   # See in the folder "www/Translations"

# Datacenter city and/or country
DATACENTER_LOCATION = '',

# Track ID of uptime button in https://app.statuscake.com/button/index.php?Track=...
STATUSCAKE_ID = '',   # optional

# Support mail
EMAIL = 'support@blazar.link',

# Relative or absolute URL of static content (images, CSS, script...)
URL_STATIC = '/static',

# Available backgrounds
BACKGROUND1 = '/static/bg1.png',
BACKGROUND2 = '/static/bg2.jpg',
BACKGROUND3 = '/static/bg3.png',

# Directory where files are stored (absolute or relative path)
PATH_FILES = '../data',
# Examples :
# PATH_FILES = '/var/www/blazar/data/',
# PATH_FILES = 'C:/Users/Vince/Desktop/Blazar/',

# Database Source Name (PDO DSN)
DB_SOURCE = 'sqlite:../Blazar.db',
# Examples :
# MySQL socket --> 'mysql:unix_socket=/var/run/mysqld/mysqld.sock;dbname=Blazar'
# MySQL TCP/IP --> 'mysql:host=localhost;port=3306;dbname=Blazar'
# MS SQL Server -> 'sqlsrv:Server=localhost,1521;Database=Blazar'
# PostgreSQL ----> 'pgsql:host=localhost;port=5432;dbname=Blazar'
# Oracle DB -----> 'oci:dbname=//localhost:1521/Blazar'
# SQLite 3 ------> 'sqlite:C:/Users/Vince/Desktop/Blazar.db'

# Prefix of tables in database (if necessary)
DB_TABLES_PREFIX = '',

# Connection to database
DB_USERNAME = 'root',
DB_PASSWORD = '',

# Time limit before deleting a link
LINK_DROP_DELAY = 72,   # hours

# Warning: Never change this option after the 1st upload!
BLOCK_SIZE = 2048;   # Bytes

}