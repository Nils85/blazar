<div>
	<span class="V"><?php echo TRANS[28] ?></span>
	<br/>
	<table style="border-spacing: 20px 5px">
		<thead style="text-align:left">
			<tr><th colspan="3"><?php echo TRANS[29] ?></th></tr>
		</thead>
		<tbody style="text-align:right">
			<tr>
				<td><?php echo TRANS[30] ?></td>
				<td><?php echo $view[5] ?></td>
				<td>(<?php echo translate_size($view[6]) ?>)</td>
			</tr>
			<tr>
				<td><?php echo TRANS[31] ?></td>
				<td><?php echo translate_num($view[7]) ?></td>
				<td>(<?php echo translate_size($view_head[2]) ?>)</td>
			</tr>
			<tr>
				<td><?php echo TRANS[32] ?></td>
				<td><?php echo $view[0] ?></td>
				<td>(~ <?php echo translate_size(($view[2]*$view[3])-($view[7]*($view[3]/2))) ?>)</td>
			</tr>
		</tbody>
	</table>
	<table style="border-spacing: 20px 5px">
		<thead style="text-align:left">
			<tr><th colspan="3"><?php echo TRANS[33] ?></th></tr>
		</thead>
		<tbody style="text-align:right">
			<tr>
				<td><?php echo TRANS[34] ?></td>
				<td>
					<abbr title="<?php echo translate_num($view_head[2]),' ',TRANS[-5] ?>">
						<?php echo translate_size($view_head[2]) ?>
					</abbr>
				</td>
				<td></td>
			</tr>
			<tr>
				<td><?php echo TRANS[35] ?></td>
				<td>
					<?php $var1=($view[2]*$view[3])-($view[0]*($view[3]/2)) ?>
					<abbr title="<?php echo translate_num($var1),' ',TRANS[-5] ?>">
						~ <?php echo translate_size($var1) ?>
					</abbr>
				</td>
				<td><?php echo translate_num($view[2]),' ',TRANS[27] ?></td>
			</tr>
			<tr>
				<td><?php echo TRANS[36] ?></td>
				<td>
					<?php $var2=($view[1]*$view[3])-($view[0]*($view[3]/2)) ?>
					<abbr title="<?php echo translate_num($var2),' ',TRANS[-5] ?>">
						~ <?php echo translate_size($var2) ?>
					</abbr>
				</td>
				<td><?php echo translate_num($view[1]),' ',TRANS[27] ?></td>
			</tr>
			<tr>
				<td><?php echo TRANS[37] ?></td>
				<td>
					<abbr title="<?php echo translate_num($view[4]),' ',TRANS[-5] ?>">
						<?php echo translate_size($view[4]) ?>
					</abbr>
				</td>
				<td></td>
			</tr>
			<tr>
				<td><?php echo TRANS[38] ?></td>
				<td>
					<abbr title="<?php $var3=($view[2]*12)+($view[1]*7); echo translate_num($var3),' ',TRANS[-5] ?>">
						+ <?php echo translate_size($var3) ?>
					</abbr>
				</td>
				<td><?php echo translate_num($view[2]+$view[1]),' ',TRANS[26] ?></td>
			</tr>
			<tr>
				<td><?php echo TRANS[39] ?></td>
				<td><?php echo translate_num($view[3]),' ',TRANS[-5] ?></td>
				<td>12 <?php echo TRANS[-5] ?></td>
			</tr>
		</tbody>
	</table>
	<?php
		if (Blazar\Config::STATUSCAKE_ID)
		{
			echo TRANS[25], '<br/>',
				'<a href="https://www.statuscake.com" title="Website Uptime Monitoring">',
				'<img src="https://app.statuscake.com/button/index.php?Track=',
				Blazar\Config::STATUSCAKE_ID, '&amp;Days=30&amp;Design=2"/></a>';
		}
	?>
</div>