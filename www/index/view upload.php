<div id="A">
	<form class="Z" action="/upload/" method="post" enctype="multipart/form-data">
		<label for="a" class="V"><?php echo TRANS[18] ?></label>
		<br/><br/>
		<input type="file" id="a" name="up[]"/>
		<input type="submit" value="<?php echo TRANS[19] ?>" style="margin:0 9px"/>
		<br/>
		<span><?php echo TRANS[20], ' ', $view ?></span>
	</form>
	<span class="W"><?php echo TRANS[9] ?></span>
</div>
<div id="B" style="display:none">
	<form action="" method="post" enctype="multipart/form-data" id="C" class="Z">
		<label for="b" id="D" class="V"><?php echo TRANS[18] ?></label>
		<span id="E" class="V" style="display:none">
			Upload...<img src="<?php echo Blazar\Config::URL_STATIC ?>/orbit.gif"/>
		</span>
		<br/><br/>
		<input type="file" id="b" name="up[]" onchange="enableButton('F')"/>
		<input type="submit" id="F" value="<?php echo TRANS[19] ?>" style="margin:0 9px"
			onclick="upload()" disabled/>
		<br/>
		<span id="G"><?php echo TRANS[20], ' ', $view ?></span>
	</form>
	<span class="W">
		<?php
			if (filter_input(INPUT_SERVER,'HTTPS') == 'on')
			{ echo TRANS[21]; }
			else
			{ echo '<a href="https://', filter_input(INPUT_SERVER,'HTTP_HOST'), '">', TRANS[22], '</a>'; }
		?>
	</span>
</div>
<div id="H" style="display:none">
	<p>
		<span class="V"><?php echo TRANS[17] ?></span>
		<br/>
		<span id="K"></span>
		<br/><br/>
		<?php echo TRANS[8] ?>
		<br/><?php echo TRANS[48] ?>
		<input id="I" type="text" value="https://<?php echo filter_input(INPUT_SERVER,'HTTP_HOST') ?>?dl="
			style="width:250px" onclick="this.select()" readonly/>
		<br/><?php echo TRANS[41] ?>
		<input id="J" type="text" value="" style="width:250px" onclick="this.select()" readonly/>
	</p>
	<a href="/"><?php echo TRANS[7] ?></a>
</div>