<?php
spl_autoload_register(function($class_name) {
	require '../' . str_replace('\\', '/', $class_name) . '.php';
});

$deduplicator = Blazar\Blazar::getDeduplicator();

if (filter_has_var(INPUT_POST, 'dl') && filter_has_var(INPUT_POST, 'code'))  // Download
{
	if ($deduplicator->download(filter_input(INPUT_POST,'dl'), filter_input(INPUT_POST,'code')))
	{ exit; }
}

$stats = $deduplicator->getStats(1);
$view_head = [
	0 => Blazar\Blazar::getBackground(),
	1 => $stats['Counter1'],
	2 => $stats['Counter2']];

$language = Blazar\Config::DEFAULT_LANGUAGE;
include '../Translations/translate.php';
include '../Templates/head.php';
include 'view.php';
include '../Templates/foot.php';