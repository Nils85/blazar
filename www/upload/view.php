<?php
	if (!isset($view_head))
	{ exit; }

	include '../Translations/translate.php';
	include '../Templates/head.php';
	//TODO: add filename
?>
<div id="A0" class="F" style="display:none; padding:20px">
	<div id="C0">
		<p>
			<span style="font-size:2em"><?php echo TRANS[17] ?></span>
			<br/><br/>
			<?php echo TRANS[8] ?>
			<br/><?php echo TRANS[48] ?>
			<input id="C1" type="text" value="?dl=" style="width:250px" onclick="this.select()" readonly/>
			</br><?php echo TRANS[41] ?>
			<input id="C2" type="text" value="" style="width:250px" onclick="this.select()" readonly/>
		</p>
		<a href="/" style="text-decoration:underline"><?php echo TRANS[7] ?></a>
	</div>
	<span id="g" style="font-size:.7em">
		<?php
			if (filter_input(INPUT_SERVER,'HTTPS') == 'on')
			{ echo TRANS[21]; }
			else
			{ echo '<a href="https://', filter_input(INPUT_SERVER,'HTTP_HOST'), '">', TRANS[22], '</a>'; }
		?>
	</span>
</div>
<?php include '../Templates/foot.php' ?>