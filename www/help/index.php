<?php
spl_autoload_register(function($class_name) {
	require '../' . str_replace('\\', '/', $class_name) . '.php';
});

$stats = Blazar\Blazar::getDeduplicator()->getStats(1);
$view_head = [
	0 => Blazar\Blazar::getBackground(),
	1 => $stats['Counter1'],
	2 => $stats['Counter2']];

$language = Blazar\Config::DEFAULT_LANGUAGE;
include '../Translations/translate.php';
include '../Templates/head.php';
include 'view.php';
include '../Templates/foot.php';