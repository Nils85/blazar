<?php
spl_autoload_register(function($class_name) {
	require str_replace('\\', '/', $class_name) . '.php';
});

ignore_user_abort(true);
set_time_limit(0);

$deduplicator = Blazar\Blazar::getDeduplicator();

if (!is_dir($deduplicator->pathFolder))
{ exit; }

$path = '';
$min_size = PHP_INT_MAX;

foreach (scandir($deduplicator->pathFolder, SCANDIR_SORT_NONE) as $filename)
{
	$filepath = $deduplicator->pathFolder . $filename;

	if (strlen($filename) > 9 && is_file($filepath))  // strlen('error.log') == 9
	{
		$size = filesize($filepath);

		if ($size > 0 && $size < $min_size)
		{
			$path = $filepath;
			$min_size = $size;
		}
	}
}

try
{
	if ($path != '' && !$deduplicator->partialFileExist())
	{ $deduplicator->storeFile($path); }
}
catch (Deduplicator\DeduplicatorException $ex)
{
	trigger_error($ex->getMessage(), $ex->getCode());
}