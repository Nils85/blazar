'use strict';

if (typeof FormData != "undefined" && typeof XMLHttpRequest != "undefined")
{
	var div1 = document.getElementById('A');
	var div2 = document.getElementById('B');

	if (div1 != null && div2 != null)
	{
		div1.style.display = 'none';
		div2.style.removeProperty('display');
	}

	dedup();
}

function dedup()
{
	var xhr = new XMLHttpRequest();
	xhr.open('GET', '/dedup.php', true);
	xhr.send(null);
}

function selectUrl(select)
{
	var newParam = select.name + '=' + select.value;

	if (location.href.indexOf(newParam) == 0 && location.href.indexOf('?') > 0)
	{ location.href += '&' + newParam; }
	else
	{ location.href = '?' + newParam; }
}

function enableButton(id)
{
	document.getElementById(id).style.fontWeight = 'bold';
	document.getElementById(id).removeAttribute('disabled');
}

function upload()
{
	var form = document.getElementById('C');

	form.style.borderStyle = 'none';  // Form
	document.getElementById('E').style.display = 'inline';  // Hidden title
	document.getElementById('F').style.display = 'none';  // Submit button
	document.getElementById('D').style.display = 'none';  // Title label

	form.addEventListener('submit', uploadProgress);
}

function uploadProgress(e)
{
	e.preventDefault();  // Prevent regular form posting

	var progress = document.getElementById('G');
	var xhr = new XMLHttpRequest();

	xhr.upload.addEventListener('progress', function(event) {
		var percent = (100 * event.loaded / event.total);
		progress.innerHTML = percent.toFixed(2) + '%';
	}, false);

	xhr.addEventListener('readystatechange', function(event) {
		if (event.target.readyState === 4 && event.target.responseText)
		{
			document.getElementById('I').value += event.target.responseText.substring(0,7);  // URL
			document.getElementById('J').value = event.target.responseText.substring(7,29);  // Code
			document.getElementById('K').innerHTML = event.target.responseText.substring(29);  // Filename

			document.getElementById('B').style.display = 'none';  // Hide div with form
			document.getElementById('H').style.removeProperty('display');  // Show div with link

			dedup();
		}
	}, false);

	xhr.open('post', '/upload/ajax.php', true);
	xhr.send(new FormData(this));
}