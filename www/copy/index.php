<?php
spl_autoload_register(function($class_name) {
	require '../' . str_replace('\\', '/', $class_name) . '.php';
});

$deduplicator = Blazar\Blazar::getDeduplicator();
$view_page = 'view message.php';

if (filter_has_var(INPUT_POST, 'name'))
{
	$link = $deduplicator->copy(filter_input(INPUT_POST, 'name'),
		filter_input(INPUT_POST, 'link'), filter_input(INPUT_POST, 'code'));

	$infos = $deduplicator->getFileInfos($link['ID'], $link['Code']);

	if (isset($infos['Size']))
	{
		$view_page = 'view copied.php';
		$view = [
			0 => $link['ID'],
			1 => $link['Code'],
			2 => htmlspecialchars($infos['Name']),
			3 => 'https://' . filter_input(INPUT_SERVER, 'HTTP_HOST')];
	}
}
elseif (filter_has_var(INPUT_POST, 'link'))
{
	$link = filter_input(INPUT_POST, 'link');
	$code = filter_input(INPUT_POST, 'code');
	$file = $deduplicator->getFileInfos($link, $code);

	if (isset($file['Size']))
	{
		$view_page = 'view copy.php';
		$view = [
			0 => $link,
			1 => $file['Size'],
			2 => $code,
			3 => htmlspecialchars($file['Name'])];
	}
}

$stats = $deduplicator->getStats(1);
$view_head = [
	0 => Blazar\Blazar::getBackground(),
	1 => $stats['Counter1'],
	2 => $stats['Counter2']];

$language = Blazar\Config::DEFAULT_LANGUAGE;
include '../Translations/translate.php';
include '../Templates/head.php';
include $view_page;
include '../Templates/foot.php';