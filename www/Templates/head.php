<!DOCTYPE html>
<html lang="<?php echo $language ?>" dir="<?php echo TRANS[0] ?>">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=470">
	<title><?php echo Blazar\Config::WEBSITE_NAME ?> - <?php echo TRANS[2] ?></title>
	<link rel="icon" type="image/png" href="<?php echo Blazar\Config::URL_STATIC ?>/icon.png"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Blazar\Config::URL_STATIC ?>/style.css"/>
	<script defer src="<?php echo Blazar\Config::URL_STATIC ?>/ES5.js"></script>
</head>
<body style="background-image:url(<?php echo $view_head[0] ?>)">
	<div><div>
		<span>
			<a href="/stat/">
				<?php printf(TRANS[3],translate_num($view_head[1]),translate_size($view_head[2])) ?>
			</a>
		</span>
		<a href="/">
			<img src="<?php echo Blazar\Config::URL_STATIC ?>/icon.png"/>
			<b><?php echo Blazar\Config::WEBSITE_NAME ?></b><br/>
			<i><?php echo TRANS[2] ?></i>
		</a>
	</div></div>
	<div><div>