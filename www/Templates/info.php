<?php
	if (strlen($view_info[0]) > 32 && strpos($view_info[0], ' ') == 0)
	{ $view_info[0] = str_replace('_', ' ', $view_info[0]); }
?>
<span style="font-size:1.5em"><?php echo $view_info[0] ?></span>
<br/>
<abbr title="<?php echo translate_num($view_info[5]),' ',TRANS[-5] ?>">
	<?php echo translate_size($view_info[5]) ?>
</abbr>
<table style="border-spacing: 20px 4px">
	<thead>
		<tr><th colspan="2"><?php echo TRANS[11] ?></th></tr>
	</thead>
	<tbody>
		<tr>
			<td><?php echo TRANS[12] ?></td>
			<td><?php echo $view_info[2] ?></td>
		</tr>
		<tr>
			<td><?php echo TRANS[13] ?></td>
			<td>
				<abbr title="<?php echo date(TRANS[-4],$view_info[3]) ?> (UTC)">
					<?php printf(TRANS[75],translate_interval($view_info[3])) ?>
				</abbr>
			</td>
		</tr>
		<tr>
			<td><?php echo TRANS[14] ?></td>
			<td>
				<abbr title="<?php echo date(TRANS[-4],$view_info[4]) ?> (UTC)">
					<?php printf(TRANS[75],translate_interval($view_info[4])) ?>
				</abbr>
			</td>
		</tr>
		<?php
			if ($view_info[6] != '0')
			{
				echo '<tr class="Z"><td>', TRANS[15], '</td><td><b>',
					translate_interval($view_info[6]), '</b></td></tr>';
			}
		?>
	</tbody>
</table>
<table class="Z">
	<thead>
		<tr><th colspan="2"><?php echo TRANS[16] ?></th></tr>
	</thead>
	<tbody>
		<?php
			if (isset($view_info[-1]))
			{
				echo '<tr><td>MD5 :</td><td><input type="text" value="', $view_info[-3], '" readonly/></td></tr>',
					'<tr><td>SHA1 :</td><td><input type="text" value="', $view_info[-2], '" readonly/></td></tr>',
					'<tr><td>SHA256 :</td><td><input type="text" value="', $view_info[-1], '" readonly/></td></tr>';
			}
		?>
		<tr>
			<td class="Y">SHA512 :</td>
			<td><input type="text" value="<?php echo $view_info[1] ?>" readonly/></td>
		</tr>
	</tbody>
</table>