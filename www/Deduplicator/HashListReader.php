<?php
namespace Deduplicator;
use Deduplicator\Crypto\StreamDecryptor;
use Deduplicator\Crypto\Crypto;

/**
 * Read an existing encrypted hash list.
 * @author Vincent Boursier <vincent.boursier@gmail.com>
 */
class HashListReader
{
	private $file;
	private $bufferSize;
	private $crypto;
	private $key;

	function __construct($path, $chunk_size, $key)
	{
		$this->key = $key;
		$this->crypto = new StreamDecryptor($key);
		$this->bufferSize = $chunk_size;
		$this->file = fopen($path, 'rb');  // read only
	}

	public function read()
	{
		$stream = fread($this->file, $this->bufferSize);

		if ($stream === false)
		{ return ''; }

		return $this->crypto->decrypt($stream);
	}

	/**
	 * Read the last block of the hash list.
	 * @return string (binary)
	 */
	public function readEnd()
	{
		$position = ftell($this->file);

		fseek($this->file, -$this->bufferSize, SEEK_END);
		$counter = ftell($this->file) / $this->bufferSize;
		$data = fread($this->file, $this->bufferSize);

		fseek($this->file, $position);
		return Crypto::stringDecrypt($data, $this->key, $counter);
	}
}