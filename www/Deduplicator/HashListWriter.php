<?php
namespace Deduplicator;
use Deduplicator\Crypto\StreamEncryptor;
use Deduplicator\DeduplicatorException;
use PDO;

/**
 * Build a new encrypted hash list.
 * @author Vincent Boursier <vincent.boursier@gmail.com>
 */
class HashListWriter
{
	private $file;
	private $filename;
	private $folder;
	private $bufferSize;
	private $crypto;
	private $pdo;
	private $tableStat;

	function __construct($chunk_size, $folder, PDO &$pdo_instance, $stat_table,  $filename, $key)
	{
		$this->crypto = new StreamEncryptor($key);
		$this->bufferSize = $chunk_size;
		$this->pdo = $pdo_instance;
		$this->tableStat = $stat_table;
		$this->filename = $filename;
		$this->folder = $folder;
		$this->file = fopen($folder . '.' . $filename, 'wb');  // if file exist truncate else create new
	}

	public function write($stream)
	{
		if (fwrite($this->file, $this->crypto->encrypt($stream)) != $this->bufferSize)
		{
			$exception = new DeduplicatorException('Hash list problem', $this->filename);
			$exception->addDetail('block', $stream);
			throw $exception;
		}
	}

	public function store()
	{
		fclose($this->file);

		$old_path = $this->folder . '.' . $this->filename;
		$new_path = $this->folder . $this->filename;

		if (is_file($new_path))
		{
			unlink($old_path);
		}
		else
		{
			rename($old_path, $new_path);
			$this->pdo->exec('update ' . $this->tableStat
				. ' set Counter1 = Counter1 +1, Counter2 = Counter2 + ' . $this->crypto->counter . ' where ID=2');
		}
	}

	public function drop()
	{
		fclose($this->file);
		unlink($this->folder . '.' . $this->filename);
	}
}