<?php
namespace Deduplicator;
use Deduplicator\Crypto\Crypto;
use PDO;

/**
 * Storage engine for encrypted links and file metadata.
 * @author Vincent Boursier <vincent.boursier@gmail.com>
 */
class LinkStorage
{
	const ID_LENGTH = 7;
	const OFFSET_INT4 = '2147483647';

	private $pdo;
	private $tableLink;
	private $tableFile;

	function __construct(PDO &$pdo_instance, $tables_prefix = '')
	{
		$this->pdo = $pdo_instance;
		$this->tableLink = $tables_prefix . 'Link';
		$this->tableFile = $tables_prefix . 'LinkFile';
	}

	/**
	 * Create a new link and return its ID.
	 * @param string $key
	 * @param string $name Filename
	 * @param string $checksum Binary
	 * @param string $file_id Binary
	 * @param string $sha512 Binary (optional)
	 * @return string ID
	 */
	public function create($key, $name, $checksum, $file_id, $sha512 = '')
	{
		$timestamp = time();

		$id = '';
		$sql = "insert into $this->tableLink (ID,Upload,Access,Name,FileID,Checksum) values (?,?,?,?,?,?)";
		$stmt = $this->pdo->prepare($sql);

		if ($stmt == false)  // or null
		{
			$this->initDatabase(strlen($file_id), strlen($checksum));
			$stmt = $this->pdo->prepare($sql);
		}

		if ($sha512 != '')  // or null
		{ $this->createFile($checksum, $file_id, $sha512); }

		$name = Crypto::stringEncrypt($name, $key, $timestamp);
		$file_id = Crypto::stringEncrypt($file_id, $key, $timestamp);
		$checksum = Crypto::stringEncrypt($checksum, $key, $timestamp);

		$stmt->bindParam(1, $id, PDO::PARAM_STR);
		$stmt->bindParam(2, $timestamp, PDO::PARAM_INT);
		$stmt->bindParam(3, $timestamp, PDO::PARAM_INT);
		$stmt->bindParam(4, $name, PDO::PARAM_LOB);
		$stmt->bindParam(5, $file_id, PDO::PARAM_LOB);
		$stmt->bindParam(6, $checksum, PDO::PARAM_LOB);

		do { $id = Crypto::randomAlphaNum(self::ID_LENGTH, '-_.'); }
		while (!$stmt->execute());

		return $id;
	}

	/**
	 * Add a new target file (if it not exists).
	 * @param type $key Binary
	 * @param type $id Binary
	 * @param string $sha512 Binary
	 */
	private function createFile($key, $id, $sha512)
	{
		$stmt = $this->pdo->prepare("insert into $this->tableFile (ID,SHA512) values (?,?)");
		$stmt->bindValue(1, $id, PDO::PARAM_LOB);
		$stmt->bindValue(2, Crypto::stringEncrypt($sha512, $key, $id), PDO::PARAM_LOB);
		$stmt->execute();
	}

	/**
	 * Add hashes in file metadata.
	 * @param type $id File ID (binary)
	 * @param string $key (binary)
	 * @param string $sha256 (binary)
	 * @param string $sha1 (binary)
	 * @param string $md5 (binary)
	 */
	public function updateFile($id, $key, $sha256, $sha1, $md5)
	{
		$stmt = $this->pdo->prepare(
			"update $this->tableFile set SHA256=?,SHA1=?,MD5=? where ID=? and SHA256 is null");

		$stmt->bindValue(1, Crypto::stringEncrypt($sha256, $key, $id), PDO::PARAM_LOB);
		$stmt->bindValue(2, Crypto::stringEncrypt($sha1, $key, $id), PDO::PARAM_LOB);
		$stmt->bindValue(3, Crypto::stringEncrypt($md5, $key, $id), PDO::PARAM_LOB);
		$stmt->bindValue(4, $id, PDO::PARAM_LOB);
		$stmt->execute();
	}

	/**
	 * Retrieve link informations and file metadata (and delete link if necessary).
	 * @param string $id Link ID
	 * @param string $key Link key
	 * @return array Name,Upload,Access,Remove,Download,FileID,Checksum,SHA512,SHA256,SHA1,MD5
	 */
	public function find($id, $key)
	{
		$id = $this->pdo->quote($id);

		$stmt = $this->pdo->query('select Name, Upload, Access, Remove, Download + ' . self::OFFSET_INT4
			. ' as Download, FileID, Checksum from '. $this->tableLink . ' where ID=' . $id);

		if ($stmt == false)  // or null
		{ return array(); }

		$link_infos = $stmt->fetch(PDO::FETCH_ASSOC);

		if (empty($link_infos))
		{ return array(); }

		if ($link_infos['Remove'] > 0 && $link_infos['Remove'] < time())
		{
			if ($this->pdo->exec("delete from $this->tableLink where ID = $id") == 1)  // row deleted
			{ $link_infos['Remove'] = -1; }
		}
		else
		{
			$this->pdo->exec('update '. $this->tableLink . ' set Access=' . time() . ' where ID=' . $id);
		}

		$link_infos['FileID'] = Crypto::stringDecrypt($link_infos['FileID'], $key, $link_infos['Upload']);
		$link_infos['Checksum'] = Crypto::stringDecrypt($link_infos['Checksum'], $key, $link_infos['Upload']);

		$file_infos = $this->findFile($link_infos['FileID'], $link_infos['Checksum']);

		if (empty($file_infos))  // bad key
		{ return array(); }

		$link_infos['Name'] = Crypto::stringDecrypt($link_infos['Name'], $key, $link_infos['Upload']);
		return array_merge($link_infos, $file_infos);
	}

	/**
	 * Retrieve and decrypt file metadata.
	 * @param string $id File ID (binary)
	 * @param string $key Checksum stored in link infos (binary)
	 * @return array SHA512,SHA256,SHA1,MD5 (binary)
	 */
	private function findFile($id, $key)
	{
		$stmt = $this->pdo->prepare("select SHA512,SHA256,SHA1,MD5 from $this->tableFile where ID=?");
		$stmt->bindValue(1, $id, PDO::PARAM_LOB);
		$stmt->execute();
		$file_infos = $stmt->fetch(PDO::FETCH_ASSOC);

		if (empty($file_infos))  // bad key
		{ return array(); }

		$file_infos['SHA512'] = Crypto::stringDecrypt($file_infos['SHA512'], $key, $id);
		$file_infos['SHA256'] = Crypto::stringDecrypt($file_infos['SHA256'], $key, $id);
		$file_infos['SHA1'] = Crypto::stringDecrypt($file_infos['SHA1'], $key, $id);
		$file_infos['MD5'] = Crypto::stringDecrypt($file_infos['MD5'], $key, $id);

		return $file_infos;
	}

	/**
	 * Mark a link to delete it later.
	 * @see find($id, $key) For real deletion
	 * @param string $id
	 * @param int $second
	 * @return bool
	 */
	public function drop($id, $second)
	{
		$stmt = $this->pdo->prepare("update $this->tableLink set Remove=? where ID=? and Remove=0");
		$stmt->bindValue(1, time() + $second, PDO::PARAM_INT);
		$stmt->bindValue(2, $id, PDO::PARAM_STR);
		return $stmt->execute();
	}

	public function incrementDownloadCounter($id)
	{
		$stmt = $this->pdo->prepare("update $this->tableLink set Download=Download+1 where ID=?");
		$stmt->bindValue(1, $id, PDO::PARAM_STR);
		$stmt->execute();
	}

	/**
	 * Create "Link" and "File" tables in the database.
	 * @param int $hash_size
	 * @param int $checksum_size
	 */
	private function initDatabase($hash_size, $checksum_size)
	{
		$varbin255 = $bin64 = $bin32 = $bin20 = $bin16 = $bin_hash = $bin_checksum = 'blob';

		switch ($this->pdo->getAttribute(PDO::ATTR_DRIVER_NAME))
		{
			case 'pgsql':
				$varbin255 = $bin64 = $bin32 = $bin20 = $bin16 = $bin_hash = $bin_checksum = 'bytea';
				break;

			case 'mysql':
			case 'sqlsrv':
				$bin16 = 'binary(16)';
				$bin20 = 'binary(20)';
				$bin32 = 'binary(32)';
				$bin64 = 'binary(64)';
				$varbin255 = 'varbinary(255)';
				$bin_hash = "binary($hash_size)";
				$bin_checksum = "binary($checksum_size)";
				break;
		}

		$this->pdo->exec("create table $this->tableLink ("
			. "ID char(" . self::ID_LENGTH . ") primary key,"
			. "Upload int not null,"
			. "Access int not null,"
			. "Download int not null default -" . self::OFFSET_INT4 . ","
			. "Remove int not null default 0,"
			. "FileID $bin_hash not null,"
			. "Checksum $bin_checksum not null,"
			. "Name $varbin255 not null)");

		$this->pdo->exec("create table $this->tableFile ("
			. "ID $bin_hash primary key,"
			. "SHA512 $bin64 not null,"
			. "SHA256 $bin32 null,"
			. "SHA1 $bin20 null,"
			. "MD5 $bin16 null)");
	}
}