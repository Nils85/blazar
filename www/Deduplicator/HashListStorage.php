<?php
namespace Deduplicator;
use PDO;

/**
 * Storage engine for encrypted hash list.
 * @author Vincent Boursier <vincent.boursier@gmail.com>
 */
class HashListStorage
{
	const TIMEOUT = 86400;  // 24 hours

	private $path;
	private $bufferSize;
	private $pdo;
	private $tableStat;

	function __construct($folder_path, $chunk_size, PDO &$pdo_instance, $table_stat)
	{
		$this->path = Deduplicator::checkFolder($folder_path);
		$this->bufferSize = $chunk_size;
		$this->pdo = $pdo_instance;
		$this->tableStat = $table_stat;
	}

	public function create($id, $key)
	{
		if (is_file($this->path . $id))
		{ return null; }

		return new HashListWriter($this->bufferSize, $this->path, $this->pdo, $this->tableStat, $id, $key);
	}

	/**
	 * Open an existing hash list (and checks its integrity).
	 * @param string $id (haxadecimal)
	 * @param string $key (binary)
	 * @return \Deduplicator\HashListReader
	 */
	public function open($id, $key)
	{
		if (is_file($this->path . $id))
		{
			$hash_list = new HashListReader($this->path . $id, $this->bufferSize, $key);
			$checksum = hash_init(Deduplicator::ALGO_CHECKSUM);

			while (($hash = $hash_list->read()) !== '')
			{ hash_update($checksum, $hash); }

			unset($hash_list);

			if (hash_final($checksum, true) === $key)
			{ return new HashListReader($this->path . $id, $this->bufferSize, $key); }
		}

		return null;
	}

	/**
	 * Count the number of block in a hash list.
	 * @param string $id
	 * @return int
	 */
	public function countBlock($id)
	{
		$path = $this->path . $id;

		if (is_file($path))
		{ return filesize($path) / $this->bufferSize; }

		return 0;
	}

	public function exist($id)
	{
		return is_file($this->path . $id);
	}

	public function partialHashExist()
	{
		foreach (scandir($this->path) as $filename)
		{
			if ($filename[0] == '.' && strlen($filename) > 2)  // ignore "." and ".."
			{
				if (time() < filemtime($this->path . $filename) + self::TIMEOUT)
				{ return true; }

				return !unlink($this->path . $filename);
			}
		}

		return false;
	}
}