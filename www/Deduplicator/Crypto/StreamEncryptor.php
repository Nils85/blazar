<?php
namespace Deduplicator\Crypto;

/**
 * Encrypt a stream in counter mode.
 * @uses OpenSSL
 * @author Vincent Boursier <vincent.boursier@gmail.com>
 */
class StreamEncryptor
{
	private $key;
	private $counter;

	function __get($name)
	{
		if ($name == 'counter')
		{ return $this->counter; }
	}

	function __construct($key, $counter = 0)
	{
		$this->counter = $counter;
		$this->key = hash(Crypto::KEY_ALGO, $key, true);
	}

	public function encrypt($data)
	{
		return openssl_encrypt($data, Crypto::CIPHER, $this->key, OPENSSL_RAW_DATA,
			hash(Crypto::IV_ALGO, $this->counter++, true));
	}
}