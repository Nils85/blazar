<?php
namespace Deduplicator\Crypto;

/**
 * Tools related to cryptography.
 * @uses OpenSSL
 * @author Vincent Boursier <vincent.boursier@gmail.com>
 */
class Crypto
{
	const COMBOHASH_LENGTH = 12;  // Bytes

	// Standard mode
	const CIPHER = 'AES-128-CFB';
	const KEY_ALGO = 'md5';  // 16 bytes
	const IV_ALGO = 'md5';  // 16 bytes

	// Secure mode
	//const CIPHER = 'AES-256-CFB';
	//const KEY_ALGO = 'ripemd256';  // 32 bytes
	//const IV_ALGO = 'md5';  // 16 bytes

	// Fast mode
	//const CIPHER = 'CAST5-CFB';
	//const KEY_ALGO = 'md5';  // 16 bytes
	//const IV_ALGO = 'fnv164';  // 8 bytes

	/**
	 * Encrypt a string independently.
	 * @param string $string
	 * @param string $key Binary
	 * @param string $iv Binary
	 * @return string Binary
	 */
	static function stringEncrypt($string, $key, $iv)
	{
		return openssl_encrypt($string, self::CIPHER, hash(self::KEY_ALGO, $key, true),
			OPENSSL_RAW_DATA, hash(self::IV_ALGO, $iv, true));
	}

	/**
	 * Decrypt a string independently.
	 * @param string $string Binary
	 * @param string $key Binary
	 * @param string $iv Binary
	 * @return string
	 */
	static function stringDecrypt($string, $key, $iv)
	{
		return openssl_decrypt($string, self::CIPHER, hash(self::KEY_ALGO, $key, true),
			OPENSSL_RAW_DATA, hash(self::IV_ALGO, $iv, true));
	}

	/**
	 * Compute a 96 bits hash with a combination of 2 algos.
	 * @param string $data Message to be hashed
	 * @param bool $raw_output Optional (default=false)
	 * @return string 12 bytes or 24 hexadecimal chars
	 */
	static function comboHash($data, $raw_output = false)
	{
		return hash('fnv164', $data, $raw_output) . hash('crc32b', $data, $raw_output);
	}

	/**
	 * Generate a random string with letters and numbers.
	 * @param int $length String length
	 * @param string $special_chars Special characters you want to add (optional)
	 * @return string
	 */
	static function randomAlphaNum($length, $special_chars = '')
	{
		$string = '';
		$chars = str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUWVXYZ') . $special_chars;
		$index = strlen($chars) -1;

		while (--$length > 0)
		{ $string .= $chars[mt_rand(0, $index)]; }

		// Never end with a special char
		return $string . $chars[mt_rand(0, 61)];  // Index max before special chars
	}

	/* Not used yet but can be usefull...
	static function benchmark($cipher, $test)
	{
		if (!function_exists('openssl_encrypt'))
		{ return 0; }

		$cipher = strtoupper($cipher);

		switch ($cipher)
		{
			case 'AES-128-CFB': $algo_key = 'md5'; $iv = 'md5'; break;  // Key & IV 16 bytes
			case 'AES-256-CFB': $algo_key = 'ripemd256'; $algo_iv = 'md5'; break;  // Key 32 bytes + IV 16 bytes
			case 'CAST5-CFB': $algo_key = 'md5'; $algo_iv = 'fnv164'; break;  // Key 16 bytes + IV 8 bytes
			default: return 0;
		}

		$key = hash($algo_key, $test, true);
		$iv = hash($algo_iv, $test, true);
		$encrypted = openssl_encrypt($test, $cipher, $key, OPENSSL_RAW_DATA, $iv);
		$start = microtime(true);

		for ($i=0; $i < 1000; ++$i)
		{ $decrypted = openssl_decrypt($encrypted, $cipher, $key, OPENSSL_RAW_DATA, $iv); }

		if ($decrypted !== $test)
		{ return 0; }

		return microtime(true) - $start;
	}
	*/
}