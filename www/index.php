<?php
spl_autoload_register(function($class_name) {
	require str_replace('\\', '/', $class_name) . '.php';
});

$view_page = 'index/view message.php';  // Error message page

$deduplicator = Blazar\Blazar::getDeduplicator();
$stats = $deduplicator->getStats(1);
$view_head = [
	0 => Blazar\Blazar::getBackground(),
	1 => $stats['Counter1'],
	2 => $stats['Counter2']];

if (Blazar\Config::MAINTENANCE_MODE)
{
	$view = true;  // Maintenance message displayed
	$view_page = 'index/view message.php';
}
elseif (filter_has_var(INPUT_GET, 'dl'))
{
	if (filter_has_var(INPUT_POST, 'code'))
	{
		$link = filter_input(INPUT_GET, 'dl');
		$code = filter_input(INPUT_POST, 'code');
		$file = $deduplicator->getFileInfos($link, $code);

		if (isset($file['Size']))
		{
			$view = [
				0 => $link,
				1 => $code];

			$view_info = [
				0 => htmlspecialchars($file['Name']),
				1 => bin2hex($file['SHA512']),
				2 => $file['Download'],
				3 => $file['Upload'],
				4 => $file['Access'],
				5 => $file['Size'],
				6 => $file['Remove']];

			if (isset($file['SHA256']))
			{
				$view_info[-1] = bin2hex($file['SHA256']);
				$view_info[-2] = bin2hex($file['SHA1']);
				$view_info[-3] = bin2hex($file['MD5']);
			}

			$view_page = 'index/view link.php';  // Link page
		}
	}
	else
	{
		$view_page = 'index/view password.php';  // Code page (for the link)
	}
}
else
{
	$view = Blazar\Blazar::getMaxUploadSize();
	$view_page = 'index/view upload.php';  // Homepage
}

$language = Blazar\Config::DEFAULT_LANGUAGE;
include 'Translations/translate.php';
include 'Templates/head.php';
include $view_page;
include 'Templates/foot.php';